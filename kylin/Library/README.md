# 部署主要和常用的库

* boost
* opengl
* libusb
* python
* ssl
* gtest
* rpc
    * grpc
    * brpc
* google
* gflags

## 1 SignalIR

```
sudo apt-get install curl
```

在安装signalIR库的时候，大部分库都已经装上了，特别是C++的，可以使用 `vcpkg` 去装许多库。
https://github.com/aspnet/SignalR-Client-Cpp

需要注意的是，装brpc：
https://github.com/apache/brpc

opengl
```
sudo apt-get install mesa-common-dev
sudo apt-get install libgl1-mesa-dev libglu1-mesa-dev
```

## 2 单独装

### 2.1 gflags

1. 在终端克隆gflags源码

```
git clone https://github.com/gflags/gflags.git
```
2. 进入gflags目录，在此目录中创建build目录
```
mkdir build && cd build
```
3. 在build目录下执行 `cmake ..`
4. 执行 `sudo make` 编译
5. 安装执行命令：`sudo make install`

### 2.2 libusb

到 http://libusb.info/ 下载需要的版本，如下载里面的 libusb-1.0.22.tar.bz2。

1. 首先，下载 `libudev-dev` 
```
sudo apt install libudev-dev
```
./configure
2. 执行 `make` 编译该安装包。
3. 执行 `make install` 安装。

如果有其他问题：http://eleaction01.spaces.eepw.com.cn/articles/article/item/176270

### 2.3 boost

到 http://www.boost.org/users/download/ 下载需要的版本，如 boost_1_63_0.tar.bz2

1. 解压命令：
```
tar -xvf boost_1_63_0.tar.bz2
```

2. 运行解压后生成的bootstrap.sh文件：
```
cd ./boost_1_63_0
./bootstrap.sh
```

3. 然后使用b2工具进行安装：
```
sudo ./b2 install
```

或者使用
```
sudo apt install libboost-system-dev
sudo apt-get install libboost-all-dev
```

### 2.4 python 

```
sudo apt-get install python-dev
```

### 2.5 png

```
sudo apt update
sudo apt install libpng12-0
sudo apt install libpng12-dev 

```

### 2.6 jsoncpp

```
sudo apt install libjsoncpp
sudo apt install libjsoncpp-dev
```